---
title: Sobre
component: ContentPage
---
No setor de tecnologia, sabemos como são rápidas as mudanças no mundo. Para acompanharmos esse ritmo, aprendemos a ser rápidos e adaptáveis para continuarmos fortes no mercado. No momento, estamos repensando o modelo de escritório tradicional e trabalhando com os membros de nossa equipe para encontrar a melhor evolução possível. Oferecemos várias oportunidades de ambiente de trabalho conectado para que os membros da equipe maximizem seus potenciais.

A flexibilidade é um aspecto fundamental da estratégia de negócios da Dell. Graças a ela, podemos competir pelos melhores talentos. Uma oportunidade de promover mudanças nas normas culturais, práticas de liderança e processos de trabalho para o benefício de indivíduos e dos negócios, e uma nova maneira de abordar o trabalho em termos da forma, do local e do momento em que ele é realizado.

Como parte da nossa estratégia de pessoas, o Local de trabalho conectado estimula os funcionários a encontrar novas formas de trabalhar e tem como foco gerar resultados de negócios melhores em vez do local em que o trabalho é feito. Nosso programa Local de trabalho conectado permite que membros de equipe elegíveis escolham o estilo que combina melhor com suas necessidades, na vida pessoal e profissional. As opções do Local de trabalho conectado com trabalho flexível que estão disponíveis para você são:
