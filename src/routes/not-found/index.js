import React from 'react';
import Layout from '../../components/Layout';
import NotFound from './NotFound';
import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'notfound.title',
    defaultMessage: 'Não encontrado!',
    description: 'Not found message',
  },
  description: {
    id: 'notfound.description',
    defaultMessage: 'Desculpe, a página que você está procurando não existe!',
    description: 'Not found description',
  },
});

function action({ intl }) {
  const title = intl.formatMessage(messages.title);
  const description = intl.formatMessage(messages.description);
  const data = { title, description };
  return {
    chunks: ['not-found'],
    title,
    component: (
      <Layout>
        <NotFound {...data} />
      </Layout>
    ),
    status: 404,
  };
}

export default action;
