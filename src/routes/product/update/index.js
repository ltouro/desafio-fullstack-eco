import React from 'react';
import Layout from '../../../components/Layout';
import ProductForm from '../ProductForm';

const title = 'Add Product';

function action(context) {
  const productId = context.params.productId;
  return {
    chunks: ['update-product'],
    title,
    component: (
      <Layout>
        <ProductForm title={title} editProductId={productId} />
      </Layout>
    ),
  };
}

export default action;
