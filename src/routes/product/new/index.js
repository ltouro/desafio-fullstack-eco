import React from 'react';
import Layout from '../../../components/Layout';
import ProductForm from '../ProductForm';

const title = 'Add Product';

function action() {
  return {
    chunks: ['new-product'],
    title,
    component: (
      <Layout>
        <ProductForm title={title} />
      </Layout>
    ),
  };
}

export default action;
