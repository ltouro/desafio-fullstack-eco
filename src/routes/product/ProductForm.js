import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import get from '../../core/accessor';
import PrivateArea from '../../components/PrivateArea/PrivateArea';
import {
  loadProduct,
  updateProduct,
  updateProductFormValues,
} from '../../actions/product';

class ProductForm extends React.Component {
  static defaultProps = {
    error: {},
    productId: '',
    name: '',
    description: '',
    factor: '',
    image: '',
    unitValue: '',
    editProductId: null,
  };
  static propTypes = {
    updateProductFormValues: PropTypes.func.isRequired,
    editProductId: PropTypes.string,
    loadProduct: PropTypes.func.isRequired,
    updateProduct: PropTypes.func.isRequired,
    error: PropTypes.shape({
      msg: PropTypes.string,
    }),
    productId: PropTypes.string.isRequired,
    name: PropTypes.string,
    factor: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    unitValue: PropTypes.number,
  };

  componentDidMount() {
    console.info(this);
    if (this.props.editProductId) {
      this.props.updateProductFormValues({
        productId: this.props.editProductId,
      });
    }
    this.props.loadProduct();
  }

  handleChange(e) {
    const id = e.target.id;
    const val = e.target.value;
    const newState = {};
    newState[id] = val;
    this.props.updateProductFormValues(newState);
  }

  saveProduct(e) {
    e.preventDefault();
    this.props.updateProduct();
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <PrivateArea>
              <h1>Novo Produto</h1>
              <hr />
              <div className="card">
                <div className="card-header">Preencha os dados do produto.</div>
                <div className="card-body">
                  {this.props.error.msg ? (
                    <div className="alert alert-danger" role="alert">
                      <strong>Oh snap!</strong> {this.props.error.msg}
                    </div>
                  ) : (
                    ''
                  )}
                  <form
                    onSubmit={e => this.saveProduct(e)}
                    id={this.props.productId}
                  >
                    <div className="form-group row">
                      <label htmlFor="name" className="col-sm-2 col-form-label">
                        Nome do produto
                      </label>
                      <div className="col-sm-10">
                        <input
                          className="form-control"
                          id="name"
                          type="text"
                          value={`${this.props.name}`}
                          onChange={e => this.handleChange(e)}
                          autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label
                        className="col-sm-2 col-form-label"
                        htmlFor="description"
                      >
                        Descrição
                      </label>
                      <div className="col-sm-10">
                        <input
                          className="form-control"
                          id="description"
                          type="textarea"
                          value={`${this.props.description}`}
                          onChange={e => this.handleChange(e)}
                          name="description"
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <label
                        className="col-sm-2 col-form-label"
                        htmlFor="image"
                      >
                        URL da Imagem
                      </label>
                      <div className="col-sm-10">
                        <input
                          className="form-control"
                          id="image"
                          type="text"
                          value={`${this.props.image}`}
                          onChange={e => this.handleChange(e)}
                          name="image"
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <label
                        className="col-sm-2 col-form-label"
                        htmlFor="image"
                      >
                        Fator
                      </label>
                      <div className="col-sm-10">
                        <select
                          className="form-control"
                          id="factor"
                          type="factor"
                          value={`${this.props.factor}`}
                          onChange={e => this.handleChange(e)}
                          name="factor"
                        >
                          <option value="" name="" />
                          <option value="A" name="A">
                            A
                          </option>
                          <option value="B" name="B">
                            B
                          </option>
                          <option value="C" name="C">
                            C
                          </option>
                        </select>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label
                        className="col-sm-2 col-form-label"
                        htmlFor="unitPrice"
                      >
                        Preço Unitário
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="number"
                          step="0.01"
                          className="form-control"
                          id="unitValue"
                          value={`${this.props.unitValue}`}
                          onChange={e => this.handleChange(e)}
                          name="unitValue"
                        />
                      </div>
                    </div>

                    <button className="btn btn-primary">Salvar</button>
                  </form>
                </div>
              </div>
            </PrivateArea>
          </div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  error: get(state, 'product.error'),
  productId: get(state, 'product.productId'),
  name: get(state, 'product.name'),
  description: get(state, 'product.description'),
  unitValue: get(state, 'product.unitValue'),
  image: get(state, 'product.image'),
  factor: get(state, 'product.factor'),
});

const mapDispatch = {
  updateProductFormValues,
  updateProduct,
  loadProduct,
};

export default connect(mapState, mapDispatch)(ProductForm);
