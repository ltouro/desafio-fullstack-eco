import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import MaskedInput from 'react-text-mask';
import s from './Checkout.css';
import {
  updateStep,
  updateCheckoutFormValues,
  searchZipCode,
  placeOrder,
} from '../../actions/checkout';
import { loadProfile } from '../../actions/account';
import { loadCart } from '../../actions/cart';
import get from '../../core/accessor';
import shop from '../../core/shop';
import CartList from '../../components/CartList/CartList';

class Contact extends React.Component {
  static defaultProps = {
    currentFormStep: 1,
    maxFormStep: 1,
    name: '',
    email: '',
    docId: '',
    phoneNo: '',
    cellNo: '',
    state: '',
    city: '',
    street: '',
    number: '',
    neigh: '',
    complement: '',
    zipCode: '',
    cardNo: '',
    cardHolder: '',
    cvv: '',
    expDate: '',
    error: {},
    items: [],
  };

  static propTypes = {
    loadProfile: PropTypes.func.isRequired,
    loadCart: PropTypes.func.isRequired,
    updateStep: PropTypes.func.isRequired,
    placeOrder: PropTypes.func.isRequired,
    searchZipCode: PropTypes.func.isRequired,
    updateCheckoutFormValues: PropTypes.func.isRequired,
    currentFormStep: PropTypes.number.isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        product: PropTypes.shape({
          name: PropTypes.string.isRequired,
          unitValue: PropTypes.number.isRequired,
        }),
        productId: PropTypes.string.isRequired,
        factor: PropTypes.string,
        image: PropTypes.string,
        description: PropTypes.string,
        name: PropTypes.string,
      }),
    ).isRequired,
    maxFormStep: PropTypes.number.isRequired,
    error: PropTypes.shape({
      msg: PropTypes.string,
    }).isRequired,
    name: PropTypes.string,
    email: PropTypes.string,
    docId: PropTypes.string,
    phoneNo: PropTypes.string,
    cellNo: PropTypes.string,
    state: PropTypes.string,
    city: PropTypes.string,
    street: PropTypes.string,
    number: PropTypes.string,
    neigh: PropTypes.string,
    complement: PropTypes.string,
    zipCode: PropTypes.string,
    cardHolder: PropTypes.string,
    cardNo: PropTypes.string,
    cvv: PropTypes.string,
    expDate: PropTypes.string,
  };

  componentDidMount() {
    this.props.loadProfile();
    this.props.loadCart();
  }

  setProgressStep(step) {
    if (step <= this.props.maxFormStep) this.props.updateStep(step);
  }

  handleChange(e) {
    const id = e.target.id;
    const val = e.target.value;
    const newState = {};
    newState[id] = val;
    this.props.updateCheckoutFormValues(newState);
  }

  handleZipChange(e) {
    this.handleChange(e);
    const zip = e.target.value.split(' ').join('');
    if (zip.length === 9) this.props.searchZipCode(zip);
  }

  saveStep1Form(e) {
    e.preventDefault();
    this.props.updateCheckoutFormValues({
      error: {},
    });
    const name = this.props.name.length;
    const docId = this.props.docId.length;
    const phoneNo = this.props.phoneNo.length;
    const cellNo = this.props.cellNo.length;
    const email = this.props.email.length;

    if (name && docId && phoneNo && cellNo && email) {
      this.props.updateStep(2);
    } else {
      this.props.updateCheckoutFormValues({
        error: { msg: 'Preencha todos os dados antes de prosseguir.' },
      });
    }
  }

  saveStep2Form(e) {
    e.preventDefault();
    this.props.updateCheckoutFormValues({
      error: {},
    });
    const zip = this.props.zipCode.length;
    const street = this.props.street.length;
    const complement = this.props.complement.length;
    const neigh = this.props.neigh.length;
    const city = this.props.city.length;
    const state = this.props.state.length;

    if (zip && street && complement && neigh && city && state) {
      this.props.updateStep(3);
    } else {
      this.props.updateCheckoutFormValues({
        error: { msg: 'Preencha todos os dados antes de prosseguir.' },
      });
    }
  }

  saveStep3Form() {
    this.props.updateStep(4);
  }

  saveStep4Form(e) {
    e.preventDefault();
    this.props.updateCheckoutFormValues({
      error: {},
    });
    const cardHolder = this.props.cardHolder.length;
    const cartNo = this.props.cardNo.length;
    const expDate = this.props.expDate.length;
    const cvv = this.props.cvv.length;

    if (cardHolder && cartNo && expDate && cvv) {
      console.info('place order');
      this.props.placeOrder();
    } else {
      this.props.updateCheckoutFormValues({
        error: { msg: 'Preencha todos os dados antes de prosseguir.' },
      });
    }
  }

  /* eslint-disable class-methods-use-this */
  renderCheckoutProgress(step) {
    const activated = `${s.visited} ${s.active}`;
    const step1Activated = step === 1 ? activated : '';
    const step2Activated = step === 2 ? activated : '';
    const step3Activated = step === 3 ? activated : '';
    const step4Activated = step === 4 ? activated : '';
    return (
      <div className={`${s.checkoutProgress}`}>
        <div className="container">
          <div>
            <ul className={`${s.checkoutProgressBar}`}>
              <li className={step1Activated}>
                <a
                  role="button"
                  tabIndex={0}
                  onClick={() => this.setProgressStep(1)}
                >
                  <span className={`${s.progressStedBadge}`}>1</span>
                  <span className={`${s.progressStedLabel}`}>
                    Dados pessoais
                  </span>
                </a>
              </li>
              <li className={step2Activated}>
                <a
                  role="button"
                  tabIndex={0}
                  onClick={() => this.setProgressStep(2)}
                >
                  <span className={`${s.progressStedBadge}`}>2</span>
                  <span className={`${s.progressStedLabel}`}>Endereço</span>
                </a>
              </li>
              <li className={step3Activated}>
                <a
                  role="button"
                  tabIndex={0}
                  onClick={() => this.setProgressStep(3)}
                >
                  <span className={`${s.progressStedBadge}`}>3</span>
                  <span className={`${s.progressStedLabel}`}>Confirmação</span>
                </a>
              </li>
              <li className={step4Activated}>
                <a
                  role="button"
                  tabIndex={0}
                  onClick={() => this.setProgressStep(4)}
                >
                  <span className={`${s.progressStedBadge}`}>4</span>
                  <span className={`${s.progressStedLabel}`}>Pagamento</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }

  renderStepForm(form) {
    switch (form) {
      default:
      case 1:
        return this.renderStep1Form();
      case 2:
        return this.renderStep2Form();
      case 3:
        return this.renderStep3Form();
      case 4:
        return this.renderStep4Form();
    }
  }

  renderStep1Form() {
    const phoneNoMask = shop.getPhoneMask(this.props.phoneNo);
    const cellNoMask = shop.getPhoneMask(this.props.cellNo);

    return (
      <div className="card">
        <div className="card-header">Preencha seus dados cadastrais.</div>
        <div className="card-body">
          {this.props.error.msg ? (
            <div className="alert alert-danger" role="alert">
              <strong>Oh snap!</strong> {this.props.error.msg}
            </div>
          ) : (
            ''
          )}
          <form onSubmit={e => this.saveStep1Form(e)}>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-2 col-form-label">
                Nome completo
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="name"
                  type="text"
                  value={`${this.props.name}`}
                  onChange={e => this.handleChange(e)}
                  autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                />
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="email">
                E-mail
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="email"
                  type="email"
                  value={`${this.props.email}`}
                  onChange={e => this.handleChange(e)}
                  name="email"
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="docId">
                Cadatro de Pessoa Física (CPF)
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={[
                    /[1-9]/,
                    /\d/,
                    /\d/,
                    '.',
                    /\d/,
                    /\d/,
                    /\d/,
                    '.',
                    /\d/,
                    /\d/,
                    /\d/,
                    '-',
                    /\d/,
                    /\d/,
                  ]}
                  className="form-control"
                  id="docId"
                  type="text"
                  name="docId"
                  value={`${this.props.docId}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="cellNo">
                Telefone Celular
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={cellNoMask}
                  className="form-control"
                  id="cellNo"
                  type="text"
                  name="cellNo"
                  value={`${this.props.cellNo}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="phoneNo">
                Telefone Residencial
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={phoneNoMask}
                  className="form-control"
                  id="phoneNo"
                  type="text"
                  name="phoneNo"
                  value={`${this.props.phoneNo}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group">
              <button className={`${s.button} btn btn-primary`}>Salvar</button>
            </div>
          </form>
        </div>
      </div>
    );
  }

  renderStep2Form() {
    return (
      <div className="card">
        <div className="card-header">Preencha o endereço de entrega.</div>
        <div className="card-body">
          {this.props.error.msg ? (
            <div className="alert alert-danger" role="alert">
              <strong>Oh snap!</strong> {this.props.error.msg}
            </div>
          ) : (
            ''
          )}
          <form method="post" onSubmit={e => this.saveStep2Form(e)}>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="zipCode">
                CEP
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={[
                    /[0-9]/,
                    /[0-9]/,
                    /[0-9]/,
                    /[0-9]/,
                    /[0-9]/,
                    '-',
                    /[0-9]/,
                    /[0-9]/,
                    /[0-9]/,
                  ]}
                  className="form-control"
                  id="zipCode"
                  value={this.props.zipCode}
                  name="zipCode"
                  onChange={e => this.handleZipChange(e)}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="street" className="col-sm-2 col-form-label">
                Endereço
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="street"
                  type="text"
                  name="street"
                  value={`${this.props.street}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="number" className="col-sm-2 col-form-label">
                Número
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="number"
                  type="text"
                  name="number"
                  value={`${this.props.number}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="email">
                Complemento
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="complement"
                  type="text"
                  name="complement"
                  value={`${this.props.complement}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="neigh">
                Bairro
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="neigh"
                  type="text"
                  name="neigh"
                  value={`${this.props.neigh}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="city">
                Cidade
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="city"
                  type="text"
                  name="city"
                  value={`${this.props.city}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="state">
                Estado
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="state"
                  type="text"
                  name="state"
                  value={`${this.props.city}`}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group">
              <button className={`${s.button} btn btn-primary`} type="submit">
                Salvar
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }

  renderStep3Form() {
    const summary = shop.getItemsSummary(this.props.items);

    return (
      <div className="card">
        <div className="card-header">
          Confirme todas as informações preenchidas.
          <span className="float-right">
            <button
              className="btn btn-outline-primary"
              onClick={() => this.setProgressStep(1)}
            >
              Editar dados
            </button>
          </span>
        </div>
        <div className="card-body">
          <div className="">
            <div className="col-md-12">
              <table className="table">
                <tbody>
                  <tr>
                    <td>Nome</td>
                    <td>{this.props.name}</td>
                  </tr>
                  <tr>
                    <td>CPF:</td>
                    <td>{this.props.docId}</td>
                  </tr>
                  <tr>
                    <td>Telefone celular:</td>
                    <td>{this.props.cellNo}</td>
                  </tr>
                  <tr>
                    <td>Telefone residencial:</td>
                    <td>{this.props.phoneNo || ''}</td>
                  </tr>
                  <tr>
                    <td>Endereço:</td>
                    <td>
                      {`${this.props.street}, ${this.props.number} - ${
                        this.props.complement
                      }
                    ${this.props.neigh}, ${this.props.city} - ${
                        this.props.state
                      }`}
                    </td>
                  </tr>

                  <tr>
                    <td>Subtotal</td>
                    <td>{summary.subTotal}</td>
                  </tr>
                  <tr>
                    <td>Desconto</td>
                    <td>{summary.discount}</td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td>{summary.total}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <CartList items={this.props.items} />
            <button
              className={`${s.button} btn btn-primary`}
              onClick={() => this.saveStep3Form()}
            >
              Confirmar
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderStep4Form() {
    return (
      <div className="card">
        <div className="card-header">
          Preencha os dados de pagamento com cartão de crédito.
        </div>
        <div className="card-body">
          {this.props.error.msg ? (
            <div className="alert alert-danger" role="alert">
              <strong>Oh snap!</strong> {this.props.error.msg}
            </div>
          ) : (
            ''
          )}
          <form method="post">
            <div className="form-group row">
              <label htmlFor="cardHolder" className="col-sm-2 col-form-label">
                Nome do titular
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="cardHolder"
                  name="cardHolder"
                  value={this.props.cardHolder}
                  type="text"
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="cardNo">
                Número do cartão
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={[
                    /[1-9]/,
                    /\d/,
                    /\d/,
                    /\d/,
                    ' ',
                    /\d/,
                    /\d/,
                    /\d/,
                    /\d/,
                    ' ',
                    /\d/,
                    /\d/,
                    /\d/,
                    /\d/,
                  ]}
                  className="form-control"
                  id="cardNo"
                  type="text"
                  name="cardNo"
                  value={this.props.cardNo}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="expDate">
                Data de Expiração
              </label>
              <div className="col-sm-10">
                <MaskedInput
                  mask={[/[1-9]/, /\d/, '/', /[1-9]/, /\d/]}
                  className="form-control"
                  id="expDate"
                  type="text"
                  name="expDate"
                  value={this.props.expDate}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>

            <div className="form-group row">
              <label className="col-sm-2 col-form-label" htmlFor="cvv">
                Código de Verificação (CVV)
              </label>
              <div className="col-sm-10">
                <input
                  className="form-control"
                  id="cvv"
                  type="text"
                  name="cvv"
                  value={this.props.cvv}
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>
            <div className="form-group">
              <button
                className={`${s.button} btn btn-primary`}
                onClick={e => this.saveStep4Form(e)}
              >
                Enviar
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            {this.renderCheckoutProgress(this.props.currentFormStep)}
          </div>
          <div className="col-md-12 pt-4">
            {this.renderStepForm(this.props.currentFormStep)}
          </div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  items: get(state, 'cart.items'),
  error: get(state, 'checkout.error'),
  docId: get(state, 'checkout.docId'),
  phoneNo: get(state, 'checkout.phoneNo'),
  cellNo: get(state, 'checkout.cellNo'),
  currentFormStep: get(state, 'checkout.step') || 1,
  maxFormStep: get(state, 'checkout.maxStep') || 1,
  zipCode: get(state, 'checkout.zipCode'),
  number: get(state, 'checkout.number'),
  name: get(state, 'checkout.name'),
  email: get(state, 'checkout.email'),
  street: get(state, 'checkout.street'),
  neigh: get(state, 'checkout.neigh'),
  complement: get(state, 'checkout.complement'),
  city: get(state, 'checkout.city'),
  state: get(state, 'checkout.state'),
  cardNo: get(state, 'checkout.cardNo'),
  cardHolder: get(state, 'checkout.cardHolder'),
  cvv: get(state, 'checkout.cvv'),
  expDate: get(state, 'checkout.expDate'),
});

const mapDispatch = {
  updateStep,
  loadProfile,
  loadCart,
  updateCheckoutFormValues,
  searchZipCode,
  placeOrder,
};

export default connect(mapState, mapDispatch)(withStyles(s)(Contact));
