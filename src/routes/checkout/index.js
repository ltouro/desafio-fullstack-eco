import React from 'react';
import { defineMessages } from 'react-intl';
import Layout from '../../components/Layout';
import Checkout from './Checkout';

const messages = defineMessages({
  title: {
    id: 'checkout.title',
    defaultMessage: 'Fechar pedido',
    description: 'Checkout - título da página',
  },
});

function action({ intl, store }) {
  const state = store.getState();
  if (!(state.user && state.user.id)) {
    return { redirect: '/login' };
  }
  const title = intl.formatMessage(messages.title);
  return {
    chunks: ['checkout'],
    title,
    component: (
      <Layout>
        <Checkout title={title} />
      </Layout>
    ),
  };
}

export default action;
