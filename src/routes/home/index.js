import React from 'react';
import Home from './Home';
import Layout from '../../components/Layout';

async function action() {
  return {
    chunks: ['home'],
    title: 'Eco-mmerce',
    component: (
      <Layout>
        <Home data={{ loading: true, products: [] }} />
      </Layout>
    ),
  };
}

export default action;
