import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';
import { loadCatalog } from '../../actions/catalog';
import { addItemToCart, loadCart } from '../../actions/cart';
import shop from '../../core/shop';

class Home extends React.Component {
  constructor() {
    super();
    this.addToCart = this.addToCart.bind(this);
    this.cartHasItem = this.cartHasItem.bind(this);
    this.getProductCartCount = this.getProductCartCount.bind(this);
  }
  componentDidMount() {
    this.props.loadCart();
    this.props.loadCatalog();
  }

  getProductCartCount(product) {
    return this.props.cart.items.filter(
      item => item.productId === product.productId,
    )[0].quantity;
  }

  addToCart(product, qty) {
    this.props.addItemToCart(product, qty);
  }

  cartHasItem(product) {
    return (
      this.props.cart &&
      this.props.cart.items &&
      this.props.cart.items.filter(item => item.productId === product.productId)
        .length
    );
  }

  render() {
    const { catalog: { loading, products } } = this.props;
    const maxDescriptionLength = 200;
    const readMoreString = '...[leia mais]';
    return (
      <div className="container">
        <h1>Produtos</h1>
        {loading || !products
          ? 'Carregando...'
          : products.map(item => (
              <div className="row" key={`${item.productId}-row`}>
                <div className="col-xs-4 col-lg-3">
                  <div className={s.thumbnail}>
                    <div className={s.itemListGroupItem}>
                      {' '}
                      <img className={s.groupImage} src={item.image} alt="" />
                    </div>
                  </div>
                </div>
                <div className="col-xs-8 col-lg-9">
                  <div className="caption">
                    <h4 className="group inner list-group-item-heading">
                      {item.name}
                    </h4>
                    <p className="group inner list-group-item-text">
                      {item.description.length > maxDescriptionLength
                        ? item.description.substring(0, maxDescriptionLength)
                        : item.description}
                      {item.description.length > maxDescriptionLength
                        ? readMoreString
                        : ''}
                    </p>
                    <div className="row">
                      <div className="col-xs-12 col-md-6">
                        <p className="lead">
                          {shop.convertFloatToMoney(item.unitValue, 'R$')}
                        </p>
                      </div>
                      <div className="col-xs-12 col-md-6">
                        {this.cartHasItem(item) ? (
                          <div className={s.toRight}>
                            <a className="btn btn-outline-primary disabled">
                              Adicionado
                            </a>{' '}
                            <div className={s.toRight}>
                              <a
                                role="button"
                                tabIndex={0}
                                className={`btn btn-outline-secondary`}
                                onClick={() =>
                                  this.addToCart(
                                    item,
                                    this.getProductCartCount(item) + 1,
                                  )
                                }
                              >
                                +
                              </a>{' '}
                              {`${this.getProductCartCount(item)}`}{' '}
                              <a
                                role="button"
                                tabIndex={0}
                                className={`btn btn-outline-secondary`}
                                onClick={() =>
                                  this.addToCart(
                                    item,
                                    this.getProductCartCount(item) - 1,
                                  )
                                }
                              >
                                -
                              </a>
                              <a
                                role="button"
                                tabIndex={0}
                                className={`${
                                  s.leftSpace
                                } btn btn-outline-danger`}
                                onClick={() => this.addToCart(item, 0)}
                              >
                                Remover
                              </a>
                            </div>
                          </div>
                        ) : (
                          <div>
                            <a
                              className={`${s.toRight} btn btn-outline-success`}
                              href="#"
                              onClick={() => this.addToCart(item, 1)}
                            >
                              Adicionar ao carrinho
                            </a>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
      </div>
    );
  }
}

Home.propTypes = {
  loadCart: PropTypes.func.isRequired,
  addItemToCart: PropTypes.func.isRequired,
  loadCatalog: PropTypes.func.isRequired,
  cart: PropTypes.shape({
    items: PropTypes.arrayOf(
      PropTypes.shape({
        productId: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
      }),
    ),
  }).isRequired,
  catalog: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        productId: PropTypes.string.isRequired,
        unitValue: PropTypes.number.isRequired,
        factor: PropTypes.string,
        image: PropTypes.string,
        description: PropTypes.string,
        name: PropTypes.string,
      }),
    ),
  }).isRequired,
};

const mapState = state => ({
  catalog: state.catalog,
  cart: state.cart,
});

const mapDispatch = {
  loadCatalog,
  addItemToCart,
  loadCart,
};

export default connect(mapState, mapDispatch, null, {
  pure: false,
})(withStyles(s)(Home));
