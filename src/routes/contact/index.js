import React from 'react';
import { defineMessages } from 'react-intl';
import Layout from '../../components/Layout';
import Contact from './Contact';

const messages = defineMessages({
  title: {
    id: 'contact.title',
    defaultMessage: 'Contact us',
    description: 'Contact page title',
  },
  email: {
    id: 'contact.email',
    defaultMessage: 'E-mail address',
    description: 'Email address, field in contact form',
  },
  name: {
    id: 'contact.name',
    defaultMessage: 'Name',
    description: 'Name, second field in contact form',
  },
  description: {
    id: 'contact.description',
    defaultMessage: 'Description',
    description: 'Message, third field in contact form',
  },
  send: {
    id: 'contact.send',
    defaultMessage: 'Enviar',
    description: 'submit button on contact form',
  },
});

function action({ intl }) {
  const title = intl.formatMessage(messages.title);
  const name = intl.formatMessage(messages.name);
  const email = intl.formatMessage(messages.email);
  const description = intl.formatMessage(messages.description);
  const send = intl.formatMessage(messages.send);
  return {
    chunks: ['contact'],
    title,
    component: (
      <Layout>
        <Contact
          title={title}
          email={email}
          name={name}
          description={description}
          send={send}
        />
      </Layout>
    ),
  };
}

export default action;
