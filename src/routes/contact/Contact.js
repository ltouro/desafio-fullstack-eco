import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Contact.css';

class Contact extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    send: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>{this.props.title}</h1>
            <form method="post">
              <div className={s.formGroup}>
                <label className={s.label} htmlFor="name">
                  {this.props.name}:
                  <input
                    className={s.input}
                    id="name"
                    type="text"
                    name="name"
                    autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                  />
                </label>
              </div>
              <div className={s.formGroup}>
                <label className={s.label} htmlFor="email">
                  {this.props.email}:
                  <input
                    className={s.input}
                    id="email"
                    type="email"
                    name="email"
                    autoFocus // eslint-disable-line jsx-a11y/no-autofocus
                  />
                </label>
              </div>
              <div className={s.formGroup}>
                <label className={s.label} htmlFor="description">
                  {this.props.description}:
                  <textarea
                    className={s.textarea}
                    id="description"
                    rows="6"
                    name="description"
                  />
                </label>
              </div>
              <div className={s.formGroup}>
                <button className={s.button} type="submit">
                  {this.props.send}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Contact);
