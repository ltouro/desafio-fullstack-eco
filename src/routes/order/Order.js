import React from 'react';
import PropTypes from 'prop-types';

class Order extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>{this.props.title}</h1>
            <p>O pagamento foi confirmado com sucesso. ID #{this.props.id}</p>
            <p>
              Você receberá um e-mail com os dados de confirmação em breve!{' '}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Order;
