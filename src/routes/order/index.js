import React from 'react';
import { defineMessages } from 'react-intl';
import Layout from '../../components/Layout';
import Order from './Order';

const messages = defineMessages({
  title: {
    id: 'order.title',
    description: 'Pedido - Status',
    defaultMessage: 'Acompanhamento do Pedido',
  },
});

function action(context) {
  const intl = context.intl;
  const title = intl.formatMessage(messages.title);
  return {
    chunks: ['order'],
    title,
    component: (
      <Layout>
        <Order title={title} id={context.params.id} />
      </Layout>
    ),
  };
}

export default action;
