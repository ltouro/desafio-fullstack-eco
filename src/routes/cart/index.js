import React from 'react';
import Cart from './Cart';
import Layout from '../../components/Layout';

async function action() {
  return {
    chunks: ['cart'],
    title: 'Carrinho de Compras',
    component: (
      <Layout>
        <Cart />
      </Layout>
    ),
  };
}

export default action;
