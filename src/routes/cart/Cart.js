import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadCart } from '../../actions/cart';
import shop from '../../core/shop';
import Link from '../../components/Link';
import CartList from '../../components/CartList/CartList';
import CartSummary from '../../components/CartList/CartSummary';

class Cart extends React.Component {
  componentDidMount() {
    this.props.loadCart();
  }
  /* eslint-disable no-nested-ternary */
  render() {
    const { data: { loading, items } } = this.props;
    const summary = shop.getItemsSummary(items);
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Carrinho</h1>
          </div>
          {loading ? (
            'Carregando...'
          ) : !items.length ? (
            <div className="col-md-12">Nenhum item no carrinho.</div>
          ) : (
            <div className="col-md-12">
              <CartList items={items} />
              <hr />
              <div className="row">
                <div className={`col-md-6`}>
                  <CartSummary summary={summary} />
                </div>
                <div className="col-md-6">
                  <div className="d-flex justify-content-center">
                    <div className="p-4">
                      <Link
                        className="btn btn-primary float-right"
                        to="/checkout"
                      >
                        Finalizar compra
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

Cart.propTypes = {
  loadCart: PropTypes.func.isRequired,
  data: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        product: PropTypes.shape({
          name: PropTypes.string.isRequired,
          unitValue: PropTypes.number.isRequired,
        }),
        productId: PropTypes.string.isRequired,
        factor: PropTypes.string,
        image: PropTypes.string,
        description: PropTypes.string,
        name: PropTypes.string,
      }),
    ),
  }).isRequired,
};

const mapState = state => ({
  data: {
    items: state.cart.items,
    loading: false,
  },
});

const mapDispatch = {
  loadCart,
};

export default connect(mapState, mapDispatch)(Cart);
