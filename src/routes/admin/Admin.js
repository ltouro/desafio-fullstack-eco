import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrivateArea from '../../components/PrivateArea/PrivateArea';
import Link from '../../components/Link';
import { loadCatalog } from '../../actions/catalog';
import shop from '../../core/shop';

class Admin extends React.Component {
  static defaultProps = {
    products: {},
  };

  static propTypes = {
    loadCatalog: PropTypes.func.isRequired,
    catalog: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      products: PropTypes.arrayOf(
        PropTypes.shape({
          productId: PropTypes.string.isRequired,
          unitValue: PropTypes.number.isRequired,
          factor: PropTypes.string,
          image: PropTypes.string,
          description: PropTypes.string,
          name: PropTypes.string,
        }),
      ),
    }).isRequired,
  };

  componentDidMount() {
    this.props.loadCatalog();
  }

  render() {
    const { catalog: { loading, products } } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <PrivateArea>
              <h2 className="mt-2">Painel de administração</h2>
              <hr />
              <h3>Produtos</h3>
              <table className="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Valor unitário</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tbody>
                  {loading || !products
                    ? ''
                    : products.map(item => (
                        <tr key={`${item.productId}-row`}>
                          <td>{item.productId}</td>
                          <td>{item.name}</td>
                          <td>
                            {shop.convertFloatToMoney(item.unitValue, 'R$ ')}
                          </td>
                          <td>
                            <Link
                              to={`/product/${item.productId}`}
                              role="button"
                              tabIndex={0}
                              className={`btn btn-outline-primary`}
                            >
                              Editar
                            </Link>
                            {'  '}
                            <a
                              role="button"
                              tabIndex={0}
                              className={`btn btn-outline-danger`}
                              onClick={e => console.info(e, this)}
                            >
                              Excluir
                            </a>
                          </td>
                        </tr>
                      ))}
                </tbody>
              </table>
              <Link className="btn btn-primary" to="/product/new">
                Adicionar novo
              </Link>
            </PrivateArea>
          </div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  catalog: state.catalog,
});

const mapDispatch = {
  loadCatalog,
};

export default connect(mapState, mapDispatch)(Admin);
