import React from 'react';
import { defineMessages } from 'react-intl';
import Layout from '../../components/Layout';
import Account from './Account';

const messages = defineMessages({
  welcome: {
    id: 'account.welcome',
    defaultMessage: 'Olá {name}',
    description: 'Welcome message',
  },
  title: {
    id: 'account.title',
    defaultMessage: 'Painel da Conta',
    description: 'Título da página de administração da conta',
  },
});

function action({ intl }) {
  const title = intl.formatMessage(messages.title);
  return {
    chunks: ['account'],
    title,
    component: (
      <Layout>
        <Account />
      </Layout>
    ),
  };
}

export default action;
