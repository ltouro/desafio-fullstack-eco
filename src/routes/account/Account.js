import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadProfile } from '../../actions/account';
import get from '../../core/accessor';

class Account extends React.Component {
  componentDidMount() {
    this.props.loadProfile();
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Olá {this.props.displayName}</h1>
          </div>
        </div>
      </div>
    );
  }
}

Account.propTypes = {
  loadProfile: PropTypes.func.isRequired,
  displayName: PropTypes.string.isRequired,
};

const mapState = state => ({
  displayName: get(state, 'user.profile.displayName') || 'desconhecido',
});

const mapDispatch = {
  loadProfile,
};

export default connect(mapState, mapDispatch)(Account);
