import {
  QUERY_CATALOG_START,
  QUERY_CATALOG_SUCCESS,
  QUERY_CATALOG_ERROR,
  CREATE_PRODUCT_START,
  CREATE_PRODUCT_SUCCESS,
  CREATE_PRODUCT_ERROR,
} from '../constants';

export default function catalog(state = null, action) {
  if (state === null) {
    return {
      loading: true,
    };
  }
  switch (action.type) {
    case QUERY_CATALOG_SUCCESS: {
      return {
        loading: false,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
