import {
  CHECKOUT_UPDATED,
  ORDER_PLACED,
  ORDER_PLACED_ERROR,
} from '../constants';

export default function catalog(state = null, action) {
  if (state === null) {
    return {
      step: 1,
      maxStep: 1,
    };
  }

  switch (action.type) {
    case CHECKOUT_UPDATED:
    case ORDER_PLACED:
    case ORDER_PLACED_ERROR: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
