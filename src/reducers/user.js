import { PROFILE_UPDATED } from '../constants';

export default function user(state = {}, action) {
  switch (action.type) {
    case PROFILE_UPDATED:
      return {
        ...state,
        profile: action.payload,
      };
    default:
      return state;
  }
}
