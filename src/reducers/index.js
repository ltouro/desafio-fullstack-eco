import { combineReducers } from 'redux';
import user from './user';
import runtime from './runtime';
import intl from './intl';
import catalog from './catalog';
import cart from './cart';
import checkout from './checkout';
import product from './product';

export default function createRootReducer({ apolloClient }) {
  return combineReducers({
    apollo: apolloClient.reducer(),
    user,
    runtime,
    intl,
    catalog,
    cart,
    checkout,
    product,
  });
}
