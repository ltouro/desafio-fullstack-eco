import { PRODUCT_UPDATED, PRODUCT_UPDATE_ERROR } from '../constants';

export default function product(state = null, action) {
  if (state === null) {
    return {};
  }
  switch (action.type) {
    case PRODUCT_UPDATED:
    case PRODUCT_UPDATE_ERROR: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
