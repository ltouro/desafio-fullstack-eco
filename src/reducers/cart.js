import { CART_UPDATED, CART_CHECKIN, CART_CHECKOUT } from '../constants';

export default function cart(state = null, action) {
  if (state === null) {
    return {
      initialNow: Date.now(),
    };
  }
  switch (action.type) {
    case CART_UPDATED:
    case CART_CHECKIN:
    case CART_CHECKOUT: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
