/* eslint-disable max-len */

if (process.env.BROWSER) {
  throw new Error(
    'Do not import `config.js` from inside the client-side code.',
  );
}

module.exports = {
  // default locale is the first one
  locales: [
    /* @intl-code-template '${lang}-${COUNTRY}', */
    'pt-BR',
    'en-US',
    /* @intl-code-template-end */
  ],

  // Node.js app
  port: process.env.PORT || 3000,

  // API Gateway
  api: {
    // API URL to be used in the client-side code
    clientUrl: process.env.API_CLIENT_URL || '',
    // API URL to be used in the server-side code
    serverUrl:
      process.env.API_SERVER_URL ||
      `http://localhost:${process.env.PORT || 3000}`,
  },

  // Database
  databaseUrl: process.env.DATABASE_URL || 'sqlite:database.sqlite',

  // Web analytics
  analytics: {
    // https://analytics.google.com/
    googleTrackingId: process.env.GOOGLE_TRACKING_ID, // UA-XXXXX-X
  },

  // Authentication
  auth: {
    jwt: { secret: process.env.JWT_SECRET || 'Eco-commerce' },

    // https://developers.facebook.com/
    facebook: {
      id: process.env.FACEBOOK_APP_ID || '1382603678663330',
      secret:
        process.env.FACEBOOK_APP_SECRET || 'f628fc83d8e842df823fe281d4836498',
    },

    // https://cloud.google.com/console/project
    google: {
      id:
        process.env.GOOGLE_CLIENT_ID ||
        '1014228329285-19c03o2pj7rl81h0afhh6ohtk3n61cmr.apps.googleusercontent.com',
      secret: process.env.GOOGLE_CLIENT_SECRET || 'NPa7tlxraWHd3yJF42qEvX7O',
    },

    // https://apps.twitter.com/
    twitter: {
      key: process.env.TWITTER_CONSUMER_KEY || 'kPjJdxxSRe8xABpqopjqESdgR',
      secret:
        process.env.TWITTER_CONSUMER_SECRET ||
        'hRzHKm8us7oEnTbIVnwuxWhVtq3EDiWcKNCmcgSafsqxmlPn9W',
    },
  },

  checkout: {
    pagarMeApiKey:
      process.env.PAGAR_ME_API_KEY || 'ak_test_7cL7hQy7NMlPfqPzyBJeWUCMC1ONHD',
    pagarMeApiUrl:
      process.env.PAGAR_ME_API_URL || 'https://api.pagar.me/1/transactions',
    pagarMeApiKeyEncryptionKey:
      process.env.PAGAR_ME_API_ENCRYPTION_KEY ||
      'ek_test_vRBFTUHdCJcihimdYgSinHR10aDPWA',
  },
};
