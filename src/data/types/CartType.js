import {
  GraphQLObjectType as ObjectType,
  GraphQLString as String,
  GraphQLList as List,
} from 'graphql';

import CartItemType from './CartItemType';

const CartType = new ObjectType({
  name: 'CartType',
  fields: {
    id: { type: String },
    items: { type: new List(CartItemType) },
  },
});

export default CartType;
