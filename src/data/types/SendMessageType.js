import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
} from 'graphql';

const SendMessageType = new ObjectType({
  name: 'SendMessage',
  fields: {
    messageId: { type: new NonNull(StringType) },
    name: { type: new NonNull(StringType) },
    message: { type: new NonNull(StringType) },
    email: { type: new NonNull(StringType) },
  },
});

export default SendMessageType;
