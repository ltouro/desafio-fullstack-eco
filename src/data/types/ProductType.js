import {
  GraphQLObjectType as ObjectType,
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
  GraphQLFloat as Float,
} from 'graphql';

const ProductType = new ObjectType({
  name: 'ProductType',
  fields: {
    productId: { type: new NonNull(StringType) },
    name: { type: new NonNull(StringType) },
    description: { type: StringType },
    image: { type: StringType },
    unitValue: { type: new NonNull(Float) },
    factor: { type: new NonNull(StringType) },
  },
});

export default ProductType;
