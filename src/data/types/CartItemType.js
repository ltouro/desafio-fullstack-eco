import {
  GraphQLObjectType as ObjectType,
  GraphQLString as String,
  GraphQLInt as Integer,
} from 'graphql';
import ProductType from './ProductType';
import { ProductEntry } from '../models';

const CartItemType = new ObjectType({
  name: 'CartItemType',
  fields: {
    cartId: { type: String },
    productId: { type: String },
    status: { type: String },
    quantity: { type: Integer },
    product: {
      type: ProductType,
      async resolve(cartItem) {
        return ProductEntry.find({
          where: {
            productId: cartItem.productId,
          },
        });
      },
    },
  },
});

export default CartItemType;
