import {
  GraphQLObjectType as ObjectType,
  GraphQLString as String,
} from 'graphql';

const OrderType = new ObjectType({
  name: 'OrderType',
  fields: {
    orderId: { type: String },
    status: { type: String },
  },
});

export default OrderType;
