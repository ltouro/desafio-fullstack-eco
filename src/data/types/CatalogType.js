import { GraphQLObjectType as ObjectType, GraphQLList as List } from 'graphql';

import ProductType from './ProductType';

const CatalogType = new ObjectType({
  name: 'CatalogType',
  fields: {
    products: { type: new List(ProductType) },
  },
});

export default CatalogType;
