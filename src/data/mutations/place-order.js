import { GraphQLNonNull, GraphQLString } from 'graphql';
import validator from 'validator';
import pagarme from 'pagarme';
import OrderType from '../types/OrderType';
import CartItemEntry from '../models/CartItemEntry';
import ProductEntry from '../models/ProductEntry';
import OrderEntry from '../models/OrderEntry';
import ValidationError from '../validation/ValidationError';
import shop from '../../core/shop';
import config from '../../config';

const shortid = require('shortid');

const pagarMeApiKey = config.checkout.pagarMeApiKey;

const placeOrder = {
  type: OrderType,
  name: 'PlaceOrder',
  description: 'Place an order',
  args: {
    cartId: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    phoneNo: { type: new GraphQLNonNull(GraphQLString) },
    cellNo: { type: new GraphQLNonNull(GraphQLString) },
    docId: { type: new GraphQLNonNull(GraphQLString) },
    zipCode: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    complement: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    state: { type: new GraphQLNonNull(GraphQLString) },
    neigh: { type: new GraphQLNonNull(GraphQLString) },
    number: { type: new GraphQLNonNull(GraphQLString) },
    cardHolder: { type: new GraphQLNonNull(GraphQLString) },
    cardNo: { type: new GraphQLNonNull(GraphQLString) },
    cvv: { type: new GraphQLNonNull(GraphQLString) },
    expDate: { type: new GraphQLNonNull(GraphQLString) },
    cardHash: { type: new GraphQLNonNull(GraphQLString) },
  },

  async resolve(_, input) {
    const errors = [];

    if (!validator.isLength(input.name, { min: 3 })) {
      errors.push({
        key: 'name',
        message: 'Please inform your name.',
        intl: 'contact.name.invalid',
      });
    }

    if (!validator.isLength(input.cartId, { min: 3 })) {
      errors.push({
        key: 'cart',
        message: 'Please inform the cartId.',
        intl: 'contact.cart.invalid',
      });
    }

    const cartItems = await CartItemEntry.findAll({
      where: {
        cartId: input.cartId,
        status: 'open',
      },
      include: {
        model: ProductEntry,
        as: 'product',
      },
    });
    if (!cartItems.length) {
      errors.push({
        key: 'cartId',
        message: 'This cart is empty.',
        intl: 'cart.cart.empty',
      });
      throw new ValidationError(errors);
    }
    if (errors.length) throw new ValidationError(errors);
    const items = cartItems.map(item => ({
      ...item.dataValues,
      product: item.product ? item.product.dataValues : {},
    }));
    const summary = shop.getSummary(items);
    const orderId = shortid.generate();
    const newOrder = {
      orderId,
      ...input,
      status: 'processing',
      ...summary,
    };

    const pagarMePayload = {
      ...shop.mapOrderToPagarMe(newOrder, items),
    };
    await pagarme.client
      .connect({ api_key: pagarMeApiKey })
      .then(client => client.transactions.create(pagarMePayload))
      .then(transaction => {
        console.info('transaction completed', transaction);
        if (transaction.status === 'refused') {
          errors.push({
            key: 'checkout.payment.refused',
            message: `Erro durante o processamento do pagamento: Verifique os dados informados e tente novamente.`,
          });
        }
      })
      .catch(apiError => {
        console.info(apiError);
        apiError.response.errors.forEach(err => {
          errors.push({
            key: err.parameter_name,
            message: `Erro durante o processamento do pagamento: Tente novamente mais tarde.`,
          });
        });
      });
    if (errors.length) {
      throw new ValidationError(errors);
    }
    console.info('placing order', orderId);
    await OrderEntry.create(newOrder);
    await CartItemEntry.update(
      {
        status: 'closed',
        orderId,
      },
      {
        where: {
          status: 'open',
          cartId: input.cartId,
        },
      },
    );
    return newOrder;
  },
};

export default placeOrder;
