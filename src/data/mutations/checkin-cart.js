import CartType from '../types/CartType';

const shortid = require('shortid');

const checkInCart = {
  type: CartType,
  name: 'CheckInCart',
  description: 'Check-in a cart',
  args: {},
  resolve: () => {
    const id = shortid.generate();
    return {
      id,
    };
  },
};

export default checkInCart;
