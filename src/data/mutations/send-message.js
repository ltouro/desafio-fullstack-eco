import { GraphQLNonNull, GraphQLString } from 'graphql';
import validator from 'validator';
import SendMessageType from '../types/SendMessageType';
import ContactEntry from '../models/ContactEntry';
import ValidationError from '../validation/ValidationError';

const shortid = require('shortid');

const sendMessage = {
  type: SendMessageType,
  name: 'SendMessage',
  description: 'Send us a message',
  args: {
    message: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
  },
  resolve: (value, input) => {
    const errors = [];
    if (!validator.isEmail(input.email)) {
      errors.push({
        key: 'email',
        message: 'Please inform a valid e-mail address.',
        intl: 'contact.email.invalid',
      });
    }

    if (!validator.isLength(input.name, { min: 3 })) {
      errors.push({
        key: 'name',
        message: 'Please inform your name.',
        intl: 'contact.name.invalid',
      });
    }

    if (!validator.isLength(input.message, { min: 5 })) {
      errors.push({
        key: 'message',
        message: 'Please inform your message (at least 5 characteres).',
        intl: 'contact.message.tooShort',
      });
    }

    if (errors.length) throw new ValidationError(errors);

    const id = shortid.generate();
    ContactEntry.create({
      messageId: id,
      ...input,
    });

    return {
      messageId: id,
      ...input,
    };
  },
};

export default sendMessage;
