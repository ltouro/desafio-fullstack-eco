import validator from 'validator';
import { GraphQLNonNull, GraphQLString } from 'graphql';
import CartType from '../types/CartType';
import CartItemEntry from '../models/CartItemEntry';
import ValidationError from '../validation/ValidationError';

const checkInCart = {
  type: CartType,
  name: 'CheckInCart',
  description: 'Check-in a cart',
  args: {
    id: { type: new GraphQLNonNull(GraphQLString) },
  },
  async resolve(value, input) {
    const errors = [];

    if (!validator.isLength(input.id, { min: 3 })) {
      errors.push({
        key: 'id',
        message: 'Please inform the cart id.',
        intl: 'cart.id.invalid',
      });
    }

    if (errors.length) throw new ValidationError(errors);

    const items = await CartItemEntry.findAll({
      cartId: input.id,
    });
    items.forEach(item => {
      const updatedItem = {
        ...item,
        status: 'closed',
      };
      CartItemEntry.update(updatedItem, {
        where: { cartItemId: item.cartItemId },
      });
    });
    return {
      id: input.id,
    };
  },
};

export default checkInCart;
