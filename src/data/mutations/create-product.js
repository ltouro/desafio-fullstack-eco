import { GraphQLNonNull, GraphQLString, GraphQLFloat } from 'graphql';
import validator from 'validator';
import ProductType from '../types/ProductType';
import ProductEntry from '../models/ProductEntry';
import ValidationError from '../validation/ValidationError';

const shortid = require('shortid');

const createProduct = {
  type: ProductType,
  name: 'CreateProduct',
  description: 'Create a product',
  args: {
    name: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    unitValue: { type: new GraphQLNonNull(GraphQLFloat) },
    factor: { type: new GraphQLNonNull(GraphQLString) },
    productId: { type: GraphQLString },
    image: { type: GraphQLString },
  },
  resolve: (value, input) => {
    const errors = [];

    if (!validator.isLength(input.name, { min: 3 })) {
      errors.push({
        key: 'name',
        message: 'Please inform your name.',
        intl: 'contact.name.invalid',
      });
    }
    if (errors.length) throw new ValidationError(errors);
    const id = input.productId ? input.productId : shortid.generate();
    const newProduct = {
      productId: id,
      ...input,
    };

    if (input.productId) {
      ProductEntry.update(newProduct, {
        where: {
          productId: input.productId,
        },
      });
    } else {
      ProductEntry.create(newProduct);
    }

    return newProduct;
  },
};

export default createProduct;
