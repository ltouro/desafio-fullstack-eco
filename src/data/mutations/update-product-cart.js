import validator from 'validator';
import { GraphQLNonNull, GraphQLString, GraphQLInt } from 'graphql';
import CartItemType from '../types/CartItemType';
import ValidationError from '../validation/ValidationError';
import ProductEntry from '../models/ProductEntry';
import CartItemEntry from '../models/CartItemEntry';

const shortid = require('shortid');

const updateProductCart = {
  type: CartItemType,
  name: 'UpdateProductCart',
  description: 'Update a product in the cart',
  args: {
    productId: { type: new GraphQLNonNull(GraphQLString) },
    cartId: { type: new GraphQLNonNull(GraphQLString) },
    quantity: { type: new GraphQLNonNull(GraphQLInt) },
  },
  async resolve(value, input) {
    const errors = [];
    if (!validator.isLength(input.productId, { min: 3 })) {
      errors.push({
        key: 'productId',
        message: 'Please inform the productId id.',
        intl: 'product.id.invalid',
      });
    }

    if (input.quantity < 0) {
      errors.push({
        key: 'quantity',
        message: 'Please inform the quantity (>=0).',
        intl: 'cart.item.quantity.invalid',
      });
    }
    if (errors.length) throw new ValidationError(errors);
    const product = await ProductEntry.findById(input.productId);
    if (!product) {
      errors.push({
        key: 'productId',
        message: 'Please inform a valid product id.',
        intl: 'cart.item.product.invalid',
      });
      throw new ValidationError(errors);
    }
    const cartEntry = {
      ...input,
      status: 'open',
      cartItemId: shortid.generate(),
    };
    const where = {
      productId: cartEntry.productId,
      cartId: cartEntry.cartId,
      status: cartEntry.status,
    };

    const existingItem = await CartItemEntry.find({
      where,
    });

    if (existingItem) {
      await existingItem.destroy();
    }

    if (input.quantity > 0) {
      await CartItemEntry.create(cartEntry);
    }
    return cartEntry;
  },
};

export default updateProductCart;
