import ProfileType from '../types/ProfileType';
import ProfileModel from '../models/UserProfile';

const profile = {
  type: ProfileType,
  async resolve({ request }) {
    let model = { id: 'none' };
    const profileEntry = await ProfileModel.findById(request.user.id);
    if (profileEntry) {
      model = {
        ...profileEntry.dataValues,
      };
    }
    return model;
  },
};

export default profile;
