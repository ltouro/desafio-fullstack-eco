import CatalogType from '../types/CatalogType';
import { ProductEntry } from '../models';

const catalog = {
  type: CatalogType,
  resolve() {
    const products = ProductEntry.findAll();
    return { products };
  },
};

export default catalog;
