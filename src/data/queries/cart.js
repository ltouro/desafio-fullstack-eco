import {
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
} from 'graphql';
import CartType from '../types/CartType';
import { CartItemEntry } from '../models';

const cart = {
  type: CartType,
  args: {
    id: { type: new NonNull(StringType) },
  },
  async resolve(parent, { id }) {
    const items = await CartItemEntry.findAll({
      where: {
        cartId: id,
        status: 'open',
      },
    });
    return { items };
  },
};

export default cart;
