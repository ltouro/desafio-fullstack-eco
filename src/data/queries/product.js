import {
  GraphQLString as StringType,
  GraphQLNonNull as NonNull,
} from 'graphql';
import ProductType from '../types/ProductType';
import { ProductEntry } from '../models';

const product = {
  type: ProductType,
  args: {
    productId: { type: new NonNull(StringType) },
  },
  async resolve(parent, { productId }) {
    const entry = await ProductEntry.find({
      where: {
        productId,
      },
    });
    console.info('load product ', productId, entry);
    return entry;
  },
};

export default product;
