import { GraphQLError } from 'graphql';

class ValidationError extends GraphQLError {
  constructor(errors) {
    super('The request is invalid.');
    this.state = errors.reduce((result, error) => {
      if (Object.prototype.hasOwnProperty.call(result, error.key)) {
        result[error.key].push({
          message: error.message,
          intl: error.intl || 'none',
        });
      } else {
        result[error.key] = [
          {
            message: error.message,
            intl: error.intl || 'none',
          },
        ];
      }
      return result;
    }, {});
  }
}

export default ValidationError;
