import DataType from 'sequelize';
import Model from '../sequelize';

const CartItemEntry = Model.define('CartItemEntry', {
  cartItemId: {
    type: DataType.STRING,
    primaryKey: true,
  },
  orderId: {
    type: DataType.STRING,
  },
  quantity: {
    type: DataType.INTEGER,
  },
  cartId: {
    type: DataType.STRING,
  },
  status: {
    type: DataType.STRING,
  },
});

export default CartItemEntry;
