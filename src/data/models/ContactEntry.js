import DataType from 'sequelize';
import Model from '../sequelize';

const ContactEntry = Model.define('ContactEntry', {
  messageId: {
    type: DataType.UUID,
    primaryKey: true,
  },
  name: {
    type: DataType.STRING,
  },
  email: {
    type: DataType.STRING,
  },
  message: {
    type: DataType.STRING,
  },
});

export default ContactEntry;
