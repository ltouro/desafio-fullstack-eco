import DataType from 'sequelize';
import Model from '../sequelize';

const ProductEntry = Model.define('ProductEntry', {
  productId: {
    type: DataType.UUID,
    primaryKey: true,
  },
  name: {
    type: DataType.STRING,
  },
  description: {
    type: DataType.STRING,
  },
  unitValue: {
    type: DataType.STRING,
  },
  image: {
    type: DataType.STRING,
  },
  factor: {
    type: DataType.STRING,
  },
});

export default ProductEntry;
