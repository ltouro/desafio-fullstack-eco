import sequelize from '../sequelize';
import User from './User';
import UserLogin from './UserLogin';
import UserClaim from './UserClaim';
import UserProfile from './UserProfile';
import ContactEntry from './ContactEntry';
import ProductEntry from './ProductEntry';
import CartItemEntry from './CartItemEntry';
import OrderEntry from './OrderEntry';

User.hasMany(UserLogin, {
  foreignKey: 'userId',
  as: 'logins',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});

CartItemEntry.belongsTo(ProductEntry, {
  foreignKey: 'productId',
  as: 'product',
});

User.hasMany(UserClaim, {
  foreignKey: 'userId',
  as: 'claims',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});

User.hasOne(UserProfile, {
  foreignKey: 'userId',
  as: 'profile',
  onUpdate: 'cascade',
  onDelete: 'cascade',
});

function sync(...args) {
  return sequelize.sync(...args);
}

export default { sync };
export {
  User,
  UserLogin,
  UserClaim,
  UserProfile,
  ContactEntry,
  ProductEntry,
  CartItemEntry,
  OrderEntry,
};
