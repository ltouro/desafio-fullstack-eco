import DataType from 'sequelize';
import Model from '../sequelize';

const OrderEntry = Model.define('OrderEntry', {
  orderId: {
    type: DataType.STRING,
    primaryKey: true,
  },
  name: {
    type: DataType.STRING,
  },
  email: {
    type: DataType.STRING,
  },
  cellNo: {
    type: DataType.STRING,
  },
  phoneNo: {
    type: DataType.STRING,
  },
  docId: {
    type: DataType.STRING,
  },
  zipCode: {
    type: DataType.STRING,
  },
  street: {
    type: DataType.STRING,
  },
  complement: {
    type: DataType.STRING,
  },
  city: {
    type: DataType.STRING,
  },
  state: {
    type: DataType.STRING,
  },
  neigh: {
    type: DataType.STRING,
  },
  number: {
    type: DataType.STRING,
  },
  status: {
    type: DataType.STRING,
  },
  cartId: {
    type: DataType.STRING,
  },
  subTotal: {
    type: DataType.DOUBLE,
  },
  discount: {
    type: DataType.DOUBLE,
  },
  total: {
    type: DataType.DOUBLE,
  },
  cardHash: {
    type: DataType.STRING,
  },
});

export default OrderEntry;
