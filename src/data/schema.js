import {
  GraphQLSchema as Schema,
  GraphQLObjectType as ObjectType,
} from 'graphql';

import me from './queries/me';
import news from './queries/news';
import intl from './queries/intl';
import profile from './queries/profile';
import catalog from './queries/catalog';
import cart from './queries/cart';
import product from './queries/product';

import sendMessage from './mutations/send-message';
import createProduct from './mutations/create-product';
import checkInCart from './mutations/checkin-cart';
import checkOutCart from './mutations/checkout-cart';
import updateProductCart from './mutations/update-product-cart';
import placeOrder from './mutations/place-order';

const schema = new Schema({
  mutation: new ObjectType({
    name: 'Mutation',
    fields: {
      sendMessage,
      createProduct,
      checkInCart,
      checkOutCart,
      updateProductCart,
      placeOrder,
    },
  }),
  query: new ObjectType({
    name: 'Query',
    fields: {
      me,
      news,
      intl,
      profile,
      catalog,
      cart,
      product,
    },
  }),
});

export default schema;
