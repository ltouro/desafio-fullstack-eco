import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class PrivateArea extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    return (
      <div>
        {!this.props.userId ? (
          <div>
            <h1>403 - Acesso negado</h1>
            <a>Clique aqui para fazer o login.</a>
          </div>
        ) : (
          <div>{this.props.children}</div>
        )}
      </div>
    );
  }
}

PrivateArea.defaultProps = {
  userId: null,
};

PrivateArea.propTypes = {
  userId: PropTypes.string,
};

const mapState = state => ({
  userId: state.user && state.user.id,
});

const mapDispatch = {};

export default connect(mapState, mapDispatch)(PrivateArea);
