import React from 'react';
import PropTypes from 'prop-types';

class CartSummary extends React.Component {
  render() {
    return (
      <table className="table">
        <tbody>
          <tr>
            <td>Subtotal</td>
            <td>{this.props.summary.subTotal}</td>
          </tr>
          <tr>
            <td>Desconto</td>
            <td>{this.props.summary.discount}</td>
          </tr>
          <tr>
            <td>Total</td>
            <td>{this.props.summary.total}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

CartSummary.propTypes = {
  summary: PropTypes.shape({
    subTotal: PropTypes.string.isRequired,
    discount: PropTypes.string.isRequired,
    total: PropTypes.string.isRequired,
  }).isRequired,
};

export default CartSummary;
