import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CartList.css';
import shop from '../../core/shop';
import get from '../../core/accessor';

class CartList extends React.Component {
  renderTable(items) {
    return !items.length ? (
      <div className="col-md-12">Nenhum item no carrinho.</div>
    ) : (
      <div className="col-md-12">
        <table className="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Produto</th>
              <th>Valor unitário</th>
              <th>Quantidade</th>
              <th>Desconto</th>
              <th>Subtotal</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item, i) => this.renderRow(item, items, i))}
          </tbody>
        </table>
      </div>
    );
  }

  /* eslint-disable class-methods-use-this */
  renderRow(item, items, i) {
    const itemDiscount = shop.getDiscount(item, items);
    const itemDiscountFactorPct = `${shop.getDiscountFactor(item, items)}%`;
    const unitValue = get(item, 'product.unitValue');
    const productUnitValueMoney = shop.convertFloatToMoney(unitValue, 'R$');
    const itemDiscountFactorMoney = shop.convertFloatToMoney(
      itemDiscount,
      'R$',
    );
    const subTotal = item.quantity * item.product.unitValue - itemDiscount;
    const subTotalMoney = shop.convertFloatToMoney(subTotal, 'R$');

    return (
      <tr key={`${item.productId}-row`}>
        <th scope="row">{i + 1}</th>
        <td>{item.product.name}</td>
        <td>{productUnitValueMoney}</td>
        <td>{item.quantity}</td>
        <td className={s.red}>
          {itemDiscountFactorMoney} ({itemDiscountFactorPct})
        </td>
        <td>{subTotalMoney}</td>
      </tr>
    );
  }

  render() {
    const { items } = this.props;
    return (
      <div className="container">
        <div className="row">{this.renderTable(items)}</div>
      </div>
    );
  }
}

CartList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        unitValue: PropTypes.number.isRequired,
      }),
      productId: PropTypes.string.isRequired,
      factor: PropTypes.string,
      image: PropTypes.string,
      description: PropTypes.string,
      name: PropTypes.string,
    }),
  ).isRequired,
};

export default withStyles(s)(CartList);
