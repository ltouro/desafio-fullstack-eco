import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Feedback.css';
import { defineMessages, FormattedMessage } from 'react-intl';

const messages = defineMessages({
  question: {
    id: 'feedback.question',
    defaultMessage: 'Envie uma pergunta',
    description: 'Feedback send a question',
  },
  error: {
    id: 'feedback.error',
    defaultMessage: 'Reporte um erro',
    description: 'Feedback report an error',
  },
});

class Feedback extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <a className={s.link} href="mail:contato@eco.comm">
            <FormattedMessage {...messages.question} />
          </a>
          <span className={s.spacer}>|</span>
          <a className={s.link} href="mail:contato@eco.comm">
            <FormattedMessage {...messages.error} />
          </a>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Feedback);
