import React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import cx from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Navigation.css';
import Link from '../Link';

const messages = defineMessages({
  about: {
    id: 'navigation.about',
    defaultMessage: 'Sobre (padrão)',
    description: 'About link in header',
  },
  cart: {
    id: 'navigation.cart',
    defaultMessage: 'Cart',
    description: 'Cart link in header',
  },
  login: {
    id: 'navigation.login',
    defaultMessage: 'Entrar (padrão)',
    description: 'Log in link in header',
  },
  or: {
    id: 'navigation.separator.or',
    defaultMessage: 'or',
    description: 'Last separator in list, lowercase "or"',
  },
  signup: {
    id: 'navigation.signup',
    defaultMessage: 'Sign up',
    description: 'Sign up link in header',
  },
  signout: {
    id: 'navigation.signout',
    defaultMessage: 'Sign out',
    description: 'Sign out link in header',
  },
  account: {
    id: 'navigation.account',
    defaultMessage: 'My account',
    description: 'my account link in header',
  },
});

class Navigation extends React.Component {
  render() {
    return (
      <div className={s.root} role="navigation">
        <Link className={s.link} to="/about">
          <FormattedMessage {...messages.about} />
        </Link>
        <Link className={s.link} to="/cart">
          <FormattedMessage {...messages.cart} />
        </Link>
        {this.props.userId ? (
          <span>
            <span className={s.spacer}> | </span>
            <Link className={s.link} to="/account">
              <FormattedMessage {...messages.account} />
            </Link>
            <span className={s.spacer}>
              <FormattedMessage {...messages.or} />
            </span>
            <a className={cx(s.link, s.highlight)} href="/logout">
              <FormattedMessage {...messages.signout} />
            </a>
          </span>
        ) : (
          <span>
            <span className={s.spacer}> | </span>
            <Link className={s.link} to="/login">
              <FormattedMessage {...messages.login} />
            </Link>
            <span className={s.spacer}>
              <FormattedMessage {...messages.or} />
            </span>
            <Link className={cx(s.link, s.highlight)} to="/login">
              <FormattedMessage {...messages.signup} />
            </Link>
          </span>
        )}
      </div>
    );
  }
}

Navigation.defaultProps = {
  userId: null,
  locale: 'pt-br',
};

Navigation.propTypes = {
  userId: PropTypes.string,
  locale: PropTypes.string,
};

function mapState(state) {
  return {
    userId: state.user ? state.user.id : null,
    locale: state.intl.locale ? state.intl.locale : 'pt-br',
  };
}

export default connect(mapState, null, null, {
  pure: false,
})(withStyles(s)(Navigation));
