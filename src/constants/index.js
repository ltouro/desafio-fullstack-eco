/* eslint-disable import/prefer-default-export */
export const SET_RUNTIME_VARIABLE = 'SET_RUNTIME_VARIABLE';
export const SET_LOCALE_START = 'SET_LOCALE_START';
export const SET_LOCALE_SUCCESS = 'SET_LOCALE_SUCCESS';
export const SET_LOCALE_ERROR = 'SET_LOCALE_ERROR';
export const SET_ACCOUNT_INFO = 'SET_ACCOUNT_INFO';
export const QUERY_CATALOG_START = 'QUERY_CATALOG_START';
export const QUERY_CATALOG_SUCCESS = 'QUERY_CATALOG_SUCCESS';
export const QUERY_CATALOG_ERROR = 'QUERY_CATALOG_ERROR';
export const CREATE_PRODUCT_START = 'CREATE_PRODUCT_START';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_ERROR = 'CREATE_PRODUCT_ERROR';
export const CART_UPDATED = 'CART_UPDATED';
export const CART_CHECKIN = 'CART_CHECKIN';
export const CART_CHECKOUT = 'CART_CHECKOUT';
export const CART_LOAD_ERROR = 'CART_LOAD_ERROR';
export const CART_UPDATE_ERROR = 'CART_UPDATE_ERROR';
export const PROFILE_UPDATED = 'PROFILE_UPDATED';
export const CHECKOUT_UPDATED = 'CHECKOUT_UPDATED';
export const ORDER_PLACED = 'ORDER_PLACED';
export const ORDER_PLACED_ERROR = 'ORDER_PLACED_ERROR';
export const PRODUCT_UPDATED = 'PRODUCT_UPDATED';
export const PRODUCT_UPDATE_ERROR = 'PRODUCT_UPDATED_ERROR';
