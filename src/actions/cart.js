/* eslint-disable import/prefer-default-export */

import {
  CART_UPDATED,
  // CART_CHECKIN,
  // CART_CHECKOUT,
  CART_UPDATE_ERROR,
  CART_LOAD_ERROR,
} from '../constants';

import query from './cart.graphql';
import mutation from './graphql/add-product-to-cart.graphql';

export function loadCart() {
  return async (dispatch, getState, { client }) => {
    try {
      const state = getState().cart;
      const { data } = await client.networkInterface.query({
        query,
        variables: {
          id: state.id,
        },
      });
      dispatch({
        type: CART_UPDATED,
        payload: {
          loading: false,
          ...data.cart,
        },
      });
    } catch (error) {
      dispatch({
        type: CART_LOAD_ERROR,
        payload: {
          error,
        },
      });
    }
    return null;
  };
}

export function addItemToCart(product, qty) {
  return async (dispatch, getState, { client }) => {
    const state = getState().cart;
    const cartId = state.id;
    const productId = product.productId;
    const quantity = qty;
    if (!cartId) {
      dispatch({
        type: CART_UPDATE_ERROR,
        payload: {
          error: 'cartId not defined',
        },
      });
    } else {
      dispatch({
        type: CART_UPDATED,
        payload: {
          loading: true,
        },
      });
      await client.mutate({
        mutation,
        variables: {
          cartId,
          productId,
          quantity,
        },
      });
      const { data } = await client.networkInterface.query({
        query,
        variables: {
          id: state.id,
        },
      });
      dispatch({
        type: CART_UPDATED,
        payload: {
          loading: false,
          ...data.cart,
        },
      });
    }
  };
}
