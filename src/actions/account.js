/* eslint-disable import/prefer-default-export */
import { PROFILE_UPDATED } from '../constants';

import query from './profile.graphql';

export function loadProfile() {
  return async (dispatch, getState, { client }) => {
    const { data } = await client.networkInterface.query({
      query,
    });
    dispatch({
      type: PROFILE_UPDATED,
      payload: data.profile,
    });
  };
}
