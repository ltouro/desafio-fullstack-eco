/* eslint-disable import/prefer-default-export */

import {
  QUERY_CATALOG_START,
  QUERY_CATALOG_SUCCESS,
  QUERY_CATALOG_ERROR,
  // CREATE_PRODUCT_START,
  // CREATE_PRODUCT_SUCCESS,
  // CREATE_PRODUCT_ERROR,
} from '../constants';

import query from './graphql/query-catalog.graphql';

export function loadCatalog() {
  return async (dispatch, getState, { client }) => {
    dispatch({
      type: QUERY_CATALOG_START,
      payload: {
        catalog: {
          loading: true,
          products: [],
        },
      },
    });

    try {
      const { data } = await client.networkInterface.query({
        query,
        variables: {},
      });
      dispatch({
        type: QUERY_CATALOG_SUCCESS,
        payload: {
          loading: false,
          ...data.catalog,
        },
      });
    } catch (error) {
      dispatch({
        type: QUERY_CATALOG_ERROR,
        payload: {
          error,
        },
      });
    }
    return null;
  };
}
