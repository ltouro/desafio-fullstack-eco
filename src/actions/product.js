/* eslint-disable import/prefer-default-export */

import { PRODUCT_UPDATED, PRODUCT_UPDATE_ERROR } from '../constants';

import query from './graphql/query-product.graphql';
import mutation from './graphql/add-product-to-catalog.graphql';

export function loadProduct() {
  return async (dispatch, getState, { client }) => {
    try {
      const state = getState().product;
      const { data } = await client.networkInterface.query({
        query,
        variables: {
          productId: state.productId,
        },
      });
      dispatch({
        type: PRODUCT_UPDATED,
        payload: {
          loading: false,
          ...data.product,
        },
      });
    } catch (error) {
      dispatch({
        type: PRODUCT_UPDATED,
        payload: {
          error,
        },
      });
    }
    return null;
  };
}

export function updateProduct() {
  return async (dispatch, getState, { client }) => {
    dispatch({
      type: PRODUCT_UPDATE_ERROR,
      payload: {
        error: {},
      },
    });

    const state = getState().product;
    await client
      .mutate({
        mutation,
        variables: {
          ...state,
        },
      })
      .then(result => {
        dispatch({
          type: PRODUCT_UPDATED,
          payload: result.data.createProduct,
        });
      })
      .catch(graphError => {
        console.info('graph error', graphError);
        dispatch({
          type: PRODUCT_UPDATE_ERROR,
          payload: {
            error: {
              msg:
                'Erro ao salvar o produto. Verifique as informações preenchidas.',
              intl: 'product.update.fail',
            },
          },
        });
      });
  };
}

export function updateProductFormValues(obj) {
  return async dispatch => {
    dispatch({
      type: PRODUCT_UPDATED,
      payload: {
        ...obj,
      },
    });
  };
}
