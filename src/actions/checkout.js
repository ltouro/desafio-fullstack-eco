/* eslint-disable import/prefer-default-export */
import fetch from 'isomorphic-fetch';
import pagarme from 'pagarme';
import {
  CHECKOUT_UPDATED,
  ORDER_PLACED,
  ORDER_PLACED_ERROR,
} from '../constants';
import placeOrderMutation from './place-order.graphql';

export function updateStep(step) {
  return async (dispatch, getState) => {
    const state = getState().checkout;
    const maxStep = state.maxStep < step ? step : state.maxStep;
    dispatch({
      type: CHECKOUT_UPDATED,
      payload: {
        step,
        maxStep,
      },
    });
  };
}

export function updateCheckoutFormValues(obj) {
  return async dispatch => {
    dispatch({
      type: CHECKOUT_UPDATED,
      payload: {
        ...obj,
      },
    });
  };
}

export function searchZipCode(zipCode) {
  return async dispatch => {
    const url = `https://viacep.com.br/ws/${zipCode}/json`;
    fetch(url)
      .then(response => response.json())
      .then(json => {
        const newState = {
          zipCode: json.cep,
          street: json.logradouro,
          neigh: json.bairro,
          complement: json.complemento,
          city: json.localidade,
          state: json.uf,
        };
        dispatch({
          type: CHECKOUT_UPDATED,
          payload: {
            ...newState,
          },
        });
      });
    dispatch({
      type: CHECKOUT_UPDATED,
      payload: {},
    });
  };
}

export function placeOrder() {
  return async (dispatch, getState, { client, history }) => {
    const state = getState();
    const checkout = state.checkout;
    const encrypt = checkout.encryptKey;
    const clearedCheckout = JSON.parse(JSON.stringify(checkout));
    const cardHash = await pagarme.client
      .connect({ encryption_key: encrypt })
      .then(ctx =>
        ctx.security.encrypt({
          card_number: checkout.cardNo,
          card_holder_name: checkout.cardHolder,
          card_expiration_date: checkout.expDate,
          card_cvv: checkout.cvv,
        }),
      );
    delete clearedCheckout.encryptKey;
    delete clearedCheckout.step;
    delete clearedCheckout.maxStep;
    delete clearedCheckout.error;
    const cartId = state.cart.id;
    const variables = {
      cartId,
      ...clearedCheckout,
      cardHash,
    };
    const response = await client
      .mutate({
        mutation: placeOrderMutation,
        variables,
      })
      .catch(graphError => ({
        error: {
          msg: 'Erro ao processar o pedido. Tente novamente mais tarde.',
          intl: 'checkout.order.fail',
          graphError,
        },
      }));
    if (response.error) {
      dispatch({
        type: ORDER_PLACED_ERROR,
        payload: {
          error: response.error,
        },
      });
      return;
    }
    const data = response.data;
    const orderId = data.placeOrder.orderId;
    if (orderId)
      dispatch({
        type: ORDER_PLACED,
        payload: {
          cartId,
          orderId,
          ...checkout,
        },
      });
    history.replace(`/order/${orderId}`);
  };
}
