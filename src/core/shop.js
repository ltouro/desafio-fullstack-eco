import get from './accessor';

const cartDiscountInvariant = {
  factors: {
    A: {
      step: 1,
      limit: 5,
    },
    B: {
      step: 5,
      limit: 15,
    },
    C: {
      step: 10,
      limit: 30,
    },
  },
};

const shop = {
  convertFloatToMoney(val, curr) {
    return `${curr} ${val
      .toFixed(2)
      .replace('.', ',')
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}`;
  },

  convertFloatToCents(val) {
    return val
      .toFixed(2)
      .replace('.', '')
      .replace(',', '');
  },

  getDiscount(item, items) {
    return (
      this.getDiscountFactor(item, items) *
      (1 / 100) *
      item.product.unitValue *
      item.quantity
    );
  },

  getOrderValue(item, items) {
    const totalDiscount = this.getDiscount(item, items);
    return item.product.unitValue - totalDiscount / item.quantity;
  },

  getDiscountFactor(item, items) {
    const factor = item.product.factor;
    const qty = items.reduce((acc, entry) => acc + entry.quantity);
    const rule = cartDiscountInvariant.factors[factor];
    if (!rule) return 0;

    const localDiscountFactor =
      rule.step * qty < rule.limit ? rule.step * qty : rule.limit;

    return localDiscountFactor;
  },

  getPhoneMask(value) {
    const phone = (value || '')
      .split(' ')
      .join('')
      .split('_')
      .join('')
      .split('-')
      .join('');

    return phone.length < 13
      ? [
          '(',
          /[1-9]/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
        ]
      : [
          '(',
          /[1-9]/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          '-',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
        ];
  },

  getCleanValue(stringVal) {
    return (stringVal || '')
      .split(' ')
      .join('')
      .split('/')
      .join('')
      .split('-')
      .join('')
      .split('(')
      .join('')
      .split(')')
      .join('');
  },

  getSummary(items) {
    const subtotalItems = items.reduce(
      (acc, item) => acc + get(item, 'product.unitValue') * item.quantity,
      0,
    );
    const discountItems = items.reduce(
      (acc, item) => acc + shop.getDiscount(item, items),
      0,
    );
    return {
      subTotal: subtotalItems,
      discount: discountItems,
      total: subtotalItems - discountItems,
    };
  },

  getItemsSummary(items) {
    const summary = this.getSummary(items);
    const subTotal = shop.convertFloatToMoney(summary.subTotal, 'R$');
    const discount = shop.convertFloatToMoney(summary.discount, 'R$');
    const total = shop.convertFloatToMoney(summary.total, 'R$');
    return {
      subTotal,
      discount,
      total,
    };
  },

  mapOrderToPagarMe(order, items) {
    const total = this.convertFloatToCents(this.getSummary(items).total);
    const pagarMeOrder = {
      amount: total,
      card_number: order.cardNo,
      card_cvv: order.cvv,
      card_expiration_date: this.getCleanValue(order.expDate),
      card_holder_name: order.name,
      customer: {
        external_id: '#3311',
        name: order.name,
        type: 'individual',
        country: 'br',
        email: order.email,
        documents: [
          {
            type: 'cpf',
            number: order.docId,
          },
        ],
        phone_numbers: [
          `+55${this.getCleanValue(order.phoneNo)}`,
          `+55${this.getCleanValue(order.phoneNo)}`,
        ],
        birthday: '1965-01-01',
      },
      billing: {
        name: order.name,
        address: {
          country: 'br',
          state: order.state,
          city: order.city,
          neighborhood: order.neigh,
          street: order.street,
          street_number: order.number,
          zipcode: this.getCleanValue(order.zipCode),
        },
      },
      shipping: {
        name: order.name,
        fee: 0,
        delivery_date: '2017-12-25',
        expedited: true,
        address: {
          country: 'br',
          state: order.state,
          city: order.city,
          neighborhood: order.neigh,
          street: order.street + order.number,
          street_number: order.complement,
          zipcode: this.getCleanValue(order.zipCode),
        },
      },
      items: [],
    };
    items.forEach(item => {
      pagarMeOrder.items.push({
        id: item.cartItemId,
        title: item.product.name,
        unit_price: this.convertFloatToCents(this.getOrderValue(item, items)),
        quantity: item.quantity,
        tangible: true,
      });
    });
    return pagarMeOrder;
  },
};

export default shop;
