export default function(obj, key) {
  return key.split('.').reduce((nestedObject, innerKey) => {
    if (nestedObject && innerKey in nestedObject) {
      return nestedObject[innerKey];
    }
    return undefined;
  }, obj);
}
