## Eco-mmerce - Universal Web App

Este trabalho foi realizado para fins de teste FullStack para TOTVS e foi baseado no projeto React Starter Kit

### Tecnologia

Node.js, Babel, Webpack, React, Apollo, Redux e GraphQL.

### Pré-requisitos:

Node ^v8.7.0
NPM ^v5.4.2
Yarn (npm install -g yarn)

### Executando

Para testar a aplicação web:
```shell
$ yarn install && yarn serve
```

Para executar a aplicação em modo de desenvolvimento (hot reload)
```shell
$ yarn install && yarn start
```

### Build

```shell
$ yarn run build --release
```

### Configuração

Necessário informar as credenciais do Facebook para realização de login.

Linux
```
export PAGAR_ME_API_KEY=""
export FACEBOOK_APP_ID=""
export FACEBOOK_APP_SECRET=""
```
Windows
```
set PAGAR_ME_API_KEY=
set FACEBOOK_APP_ID=
set FACEBOOK_APP_SECRET=
```

### Web

Para adicionar produtos:
```
http://localhost:3000/product/new
```

Para interagir com o endpoint GraphQL

```
http://localhost:3000/graphql
```

[ltouro]: ltourinho.cavalcante@gmail.com
